#pragma once
#include "Scene.h"

class SceneManager
{
public:
	SceneManager();
	~SceneManager();

	static SceneManager* GetInstance();

	SceneManager(const SceneManager& other) = delete;

	Scene* GetCurrentScene();
	void UnloadCurrentScene();
	void SetCurrentScreen(Scene* newScene, bool unloadPrevious = false, bool deletePrevious = false);
	void SetNextScene(Scene* newScene, bool unloadPrevious = false, bool deletePrevious = false);

	void UpdateCurrentScene(sf::Time currentTime, sf::Time dTime);
	void RenderCurrentScene(sf::RenderWindow& renderWindow);

	void ChangeToAwaitingScene();

private:
	Scene* currentScene;

	Scene* nextScene;
	bool unloadPreviousOnNext;
	bool deletePreviousOnNext;
};