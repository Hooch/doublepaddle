#pragma once
#include "Scene.h"
#include "GlobalDefines.h"
#include "PlayingScene.h"
#include <string>
#include <sstream>
#include <fstream>

class StartScreenScene : public Scene
{
public:
	StartScreenScene();
	~StartScreenScene();

	virtual void Load();
	virtual void Unload();
	virtual void Update(sf::Time currentTime, sf::Time dTime);
	virtual void Render(sf::RenderWindow& renderWnd);

private:
	sf::Font titleFont;
	sf::Text titleText;
	sf::Text bottomText;
	sf::Text instructionText;
	sf::View projectionView;

	sf::RectangleShape background;

};