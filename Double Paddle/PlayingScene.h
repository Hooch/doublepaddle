#pragma once
#include "Scene.h"
#include "PhysicsEntity.h"
#include "BrickEntity.h"
#include <list>
#include <Box2D/Box2D.h>
#include "GlobalDefines.h"
#include "PaddleEntity.h"
#include "BallEntity.h"
#include "CollisionDetectionCallbacks.h"
#include "GameHud.h"
#include "SceneManager.h"
#include "EndGameScene.h"

class PlayingScene: public Scene
{
public:
	PlayingScene();
	~PlayingScene();

	virtual void Load();
	virtual void Unload();
	virtual void Update(sf::Time currentTime, sf::Time dTime);
	virtual void Render(sf::RenderWindow& renderWnd);

	void HandleInput(const sf::Time& currentTime, const sf::Time& dTime);

private:
	void CreateBricks();
	BallEntity* SpawnBall(const b2Vec2& pos, const b2Vec2& dir, float speed);
	void ManageBalls(sf::Time currentTime, sf::Time dTime);
	void HandleBrickHit(PhysicsEntity* brick, PhysicsEntity* other);

	b2World world;
	float timescale;
	sf::View camera;
	GameHud gameHud;
	CollisionDetectionCallbacks collisionCollbacks;
	bool paused;
	sf::RenderTexture pauseTexture;
	sf::Sprite pauseSprite;
	
	sf::Vector2f worldSize;
	std::list<std::unique_ptr<BrickEntity>> Bricks;
	sf::Time ballSpawnTime;
	BallEntity* ball;
	bool ballNotMoving;
	bool initialBallDone;

	PaddleEntity paddleBottom;
	PaddleEntity paddleTop;
	PaddleEntity paddleLeft;
	PaddleEntity paddleRight;

	int score;
	int ballsLost;
	int bricksKilledInRow;

	sf::RectangleShape background;
};