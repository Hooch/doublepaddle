#pragma once
#include <Box2D/Box2D.h>

inline b2Vec2 operator*(const b2Vec2& vec, float num)
{
	return{ vec.x * num, vec.y * num };
}