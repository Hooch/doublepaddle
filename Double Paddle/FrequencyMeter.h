#pragma once

#include <SFML/System.hpp>

class FrequencyMeter
{
public:
	FrequencyMeter(sf::Time interval = sf::seconds(1.0));
	~FrequencyMeter() = default;

	float Update();
	float GetFps();
		
private:
	unsigned int framesElpased;
	sf::Clock clock;
	float prevoiusPeriodFps;
	sf::Time measureInterval;
};