#pragma once
#include "Entity.h"
#include <SFML/Graphics.hpp>

class DrawableEntity : public Entity
{
public:
	DrawableEntity() = default;
	virtual ~DrawableEntity() = default;
	virtual void Draw(sf::RenderWindow& renderWnd) = 0;
};