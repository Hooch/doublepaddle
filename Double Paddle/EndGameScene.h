#pragma once
#include "Scene.h"
#include "GlobalDefines.h"
#include "PlayingScene.h"
#include <string>
#include <sstream>
#include <fstream>

class EndGameScene : public Scene
{
public:
	EndGameScene();
	~EndGameScene();

	virtual void Load();
	virtual void Unload();
	virtual void Update(sf::Time currentTime, sf::Time dTime);
	virtual void Render(sf::RenderWindow& renderWnd);

	void SetValues(int score, int ballsLost);

private:
	int score;
	int ballsLost;
	int bestScore;

	sf::Font scoreFont;
	sf::Text scoreText;
	sf::Text ballsUsedText;
	sf::Text bestScoreText;
	sf::Text bottomText;
	sf::View projectionView;

	sf::RectangleShape background;
};