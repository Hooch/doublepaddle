#include "Scene.h"

Scene::Scene()
{
}

Scene::~Scene()
{
}

bool Scene::IsLoaded()
{
	return isLoaded;
}

void Scene::Load()
{
	isLoaded = true;
}

void Scene::Unload()
{
	isLoaded = false;
}
