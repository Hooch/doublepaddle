#include "FrequencyMeter.h"

FrequencyMeter::FrequencyMeter(sf::Time interval)
{
	measureInterval = interval;
	prevoiusPeriodFps = 1;
	framesElpased = 0;
	clock.restart();
}

float FrequencyMeter::Update()
{
	framesElpased++;
	sf::Time currTime = clock.getElapsedTime();

	if (currTime >= measureInterval)
	{
		prevoiusPeriodFps = framesElpased / currTime.asSeconds();
		framesElpased = 0;
		clock.restart();
	}

	return prevoiusPeriodFps;
}

float FrequencyMeter::GetFps()
{
	return prevoiusPeriodFps;
}