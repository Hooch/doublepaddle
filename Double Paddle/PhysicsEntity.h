#pragma once
#include "Entity.h"
#include "Helpers.h"
#include "Box2dExtensions.h"
#include <Box2D/Box2D.h>
#include <SFML/System.hpp>
#include "Command.h"
#include "CollisionDetectionCallbacks.h"

class PhysicsEntity;

typedef Command<b2Contact* /*contact*/, PhysicsEntity* /*A*/, PhysicsEntity* /*B*/> EndContactCallback;
typedef Command<b2Contact* /*contact*/, const b2ContactImpulse* /*impulse*/, PhysicsEntity* /*A*/, PhysicsEntity* /*B*/> PostSolveCallback;

class PhysicsEntity : public Entity
{
public:
	PhysicsEntity() : 
		body(nullptr), fixture(nullptr), 
		testList(),
		endContactCallbacks() {} 
 	virtual ~PhysicsEntity();

	virtual EntityType GetEntityType() { return EntityType::ET_PHYSICS; }
	virtual void PostSolve(PhysicsEntity& other, b2Contact* contact, const b2ContactImpulse* impulse);
	virtual void EndContact(PhysicsEntity& other, b2Contact* contact);
	virtual void Update(const sf::Time& currentTime, const sf::Time& dTime) { }

	b2Body& GetBody();
	b2Fixture& GetFixture();

	sf::Vector2f GetPosition();
	float GetRotationDeg();
	float GetRotationRad();

	//Callbacks
	void AddEndContactCallback(EndContactCallback callback);
	void RemoveEndContactCallback(EndContactCallback callback);
	void AddPostSolveCallback(PostSolveCallback callback);
	void RemovePostSolveCallback(PostSolveCallback callback);

protected:
	b2Body* body;
	b2Fixture* fixture;

private:
	std::list<int> testList;
	std::list<EndContactCallback> endContactCallbacks;
	std::list<PostSolveCallback> postSolveCallbacks;
};