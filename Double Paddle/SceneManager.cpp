#include "SceneManager.h"

SceneManager::SceneManager() : currentScene(nullptr), nextScene(nullptr)
{
	
}

SceneManager::~SceneManager()
{
}

SceneManager* SceneManager::GetInstance()
{
	static SceneManager* instance = new SceneManager();
	return instance;
}

void SceneManager::UnloadCurrentScene()
{
	if (currentScene)
		currentScene->Unload();
}

Scene* SceneManager::GetCurrentScene()
{
	return currentScene;
}

void SceneManager::SetCurrentScreen(Scene* newScene, bool unloadPrevious /*= false*/, bool deletePrevious/* = false*/)
{
	if (unloadPrevious)
		if (currentScene && currentScene->IsLoaded())
			currentScene->Unload();

	if (deletePrevious)
	{
		delete currentScene;
		currentScene = nullptr;
	}

	if (!newScene) return;

	currentScene = newScene;
	if (!currentScene->IsLoaded())
		currentScene->Load();
}

void SceneManager::RenderCurrentScene(sf::RenderWindow& renderWindow)
{
	if (currentScene && currentScene->IsLoaded())
		currentScene->Render(renderWindow);
	
}

void SceneManager::UpdateCurrentScene(sf::Time currentTime, sf::Time dTime)
{
	if (currentScene && currentScene->IsLoaded())
		currentScene->Update(currentTime, dTime);
}

void SceneManager::SetNextScene(Scene* newScene, bool unloadPrevious /*= false*/, bool deletePrevious /*= false*/)
{
	nextScene = newScene;
	unloadPreviousOnNext = unloadPrevious;
	deletePreviousOnNext = deletePrevious;
}

void SceneManager::ChangeToAwaitingScene()
{
	if (nextScene)
	{
		SetCurrentScreen(nextScene, unloadPreviousOnNext, deletePreviousOnNext);
		nextScene = nullptr;
	}
}
