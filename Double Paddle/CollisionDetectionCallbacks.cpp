#include "CollisionDetectionCallbacks.h"

void CollisionDetectionCallbacks::BeginContact(b2Contact* contact)
{

}

void CollisionDetectionCallbacks::EndContact(b2Contact* contact)
{
	auto fixtureA = contact->GetFixtureA();
	auto fixtureB = contact->GetFixtureB();

	PhysicsEntity* A = (PhysicsEntity*)fixtureA->GetUserData();
	PhysicsEntity* B = (PhysicsEntity*)fixtureB->GetUserData();

	if (A && B)
	{
		A->EndContact(*B, contact);
		B->EndContact(*A, contact);

		for (auto callback : endContactCallbacks)
		{
			callback(contact, A, B);
		}
	}
}

void CollisionDetectionCallbacks::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{
	auto fixtureA = contact->GetFixtureA();
	auto fixtureB = contact->GetFixtureB();

	PhysicsEntity* A = (PhysicsEntity*)fixtureA->GetUserData();
	PhysicsEntity* B = (PhysicsEntity*)fixtureB->GetUserData();

	if (A && B)
	{
		A->PostSolve(*B, contact, impulse);
		B->PostSolve(*A, contact, impulse);

		for (auto callback : postSolveCallbacks)
		{
			callback(contact, impulse, A, B);
		}
	}
}

void CollisionDetectionCallbacks::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{

}

void CollisionDetectionCallbacks::AddEndContactCallback(EndContactCallback callback)
{
	endContactCallbacks.push_back(callback);
	endContactCallbacks.unique();
}

void CollisionDetectionCallbacks::RemoveEndContactCallback(EndContactCallback callback)
{
	endContactCallbacks.remove(callback);
}

void CollisionDetectionCallbacks::AddPostSolveCallback(PostSolveCallback callback)
{
	postSolveCallbacks.push_back(callback);
	postSolveCallbacks.unique();
}

void CollisionDetectionCallbacks::RemovePostSolveCallback(PostSolveCallback callback)
{
	postSolveCallbacks.remove(callback);
}

