#include "Game.h"

Game::Game()
{
	sceneManager = SceneManager::GetInstance();
	isQuiting = false;
}

Game::~Game()
{
	delete window;
}

void Game::CreateRenderWindow(int w, int h, int posX, int posY)
{
	sf::VideoMode videoMode(w, h, 32);
	sf::ContextSettings contexSettings;
	contexSettings.antialiasingLevel = 8;

	window = new sf::RenderWindow(videoMode, WINDOW_NAME, sf::Style::Default, contexSettings);
	window->setVerticalSyncEnabled(true);
	window->setFramerateLimit(120);
}

void Game::Start()
{
	isQuiting = false;
	CreateRenderWindow(800, 600, 320, 240);
	auto startScreen = new StartScreenScene();
	sceneManager->SetCurrentScreen(startScreen, false, false);
	MainLoop();
}

void Game::MainLoop()
{
	gameClock.restart();

	// Fix your timestep: http://gafferongames.com/game-physics/fix-your-timestep/
	const sf::Time dTime = sf::seconds((float)(1.0 / 60));
	sf::Time currentTime = gameClock.getElapsedTime();
	sf::Time accumulator;
	sf::Time newTime = gameClock.getElapsedTime();
	sf::Time frameTime = newTime - currentTime;

	while (!isQuiting)
	{
		newTime = gameClock.getElapsedTime();
		frameTime = newTime - currentTime;
		currentTime = newTime;
		accumulator += frameTime;

		//Quiting Event thingy
		isQuiting = !window->isOpen();
		sf::Event event;

		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				isQuiting = true;
				window->close();
			}
		}
		//End of Quiting thingy thing

		while (accumulator >= dTime)
		{
			sceneManager->UpdateCurrentScene(gameTime, dTime);
			accumulator -= dTime;
			gameTime += dTime;			
		}

		window->clear();
		sceneManager->RenderCurrentScene(*window);
		window->display();

		sceneManager->ChangeToAwaitingScene();
	}
}