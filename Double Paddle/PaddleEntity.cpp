#include "PaddleEntity.h"

PaddleEntity::PaddleEntity(const b2Vec2& direction, const b2Vec2& centerPos, float totalMovement)
	: direction(direction), centerPos(centerPos), totalMovement(totalMovement),
	acceptLeftInput(true), acceptRightInput(true)
{
	this->direction.Normalize();
	size = {PADDLE_WIDTH , 0.05f};

	shape.setSize(size);
	shape.setFillColor(sf::Color::White);
	shape.setOrigin(size.x / 2, size.y / 2);
	shape.setPosition(sfVectorFromB2Vector(centerPos));
}

void PaddleEntity::Draw(sf::RenderWindow& renderWnd)
{
	shape.setPosition(sfVectorFromB2Vector(body->GetPosition()));
	shape.setRotation((float)RAD2DEG(body->GetAngle()));

	renderWnd.draw(shape);
}

void PaddleEntity::Register(b2World& world)
{
	b2BodyDef bodyDef = CreateBodyDefinition(centerPos);
	body = world.CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2);

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.restitution = 1;
	fixtureDef.userData = this;

	fixture = body->CreateFixture(&fixtureDef);
}

b2BodyDef PaddleEntity::CreateBodyDefinition(const b2Vec2& initialPosition)
{
	b2BodyDef bodyDefinition;
	bodyDefinition.type = b2_kinematicBody;
	bodyDefinition.position = initialPosition;
	bodyDefinition.linearDamping = 20.0f;
	bodyDefinition.angle = -acos(b2Dot(direction, { 1, 0 }));
	bodyDefinition.fixedRotation = true;
	bodyDefinition.bullet = true;

	return bodyDefinition;
}

void PaddleEntity::SetMaxSpeed(float maxSpeed)
{
	this->maxSpeed = maxSpeed;
}

void PaddleEntity::Update(const sf::Time& currentTime, const sf::Time& dTime)
{
	b2Vec2 oldV = body->GetLinearVelocity();
	float linearDamping = body->GetLinearDamping();

	float velocity = oldV.Length();
	float dumping = 1.0f / (1.0f + linearDamping * dTime.asSeconds());
	b2Vec2 newV = oldV * dumping;
	body->SetLinearVelocity(newV);

	RestrictMovement();
}

void PaddleEntity::MoveRight(const sf::Time& dTime)
{
	if (!acceptRightInput) return;
	b2Vec2 oldV = body->GetLinearVelocity();
	float force = moveForce * dTime.asSeconds();
	oldV += direction * force;

	if (oldV.Length() > maxSpeed)
	{
		oldV = direction * maxSpeed;
		if (dTime.asMicroseconds() < 0) oldV *= -1;
	}

	body->SetLinearVelocity(oldV);

	acceptLeftInput = true;
}

void PaddleEntity::MoveLeft(const sf::Time& dTime)
{
	if (!acceptLeftInput) return;
	
	b2Vec2 oldV = body->GetLinearVelocity();
	float force = -moveForce * dTime.asSeconds();
	oldV += direction * force;

	if (oldV.Length() > maxSpeed)
	{
		oldV = direction * maxSpeed;
		if (dTime.asMicroseconds() > 0) oldV *= -1;
	}

	body->SetLinearVelocity(oldV);

	acceptRightInput = true;
}

void PaddleEntity::RestrictMovement()
{
	//float furthestRight = (body->GetPosition() - centerPos).Length() + size.x / 2;
	//float furthestLeft = (body->GetPosition() - centerPos).Length() + size.x / 2;
	
	float furthest = (body->GetPosition() - centerPos).Length() + size.x / 2;
	b2Vec2 movDir = body->GetLinearVelocity();
	movDir.Normalize();

	bool onRightSide = b2Dot(direction, (body->GetPosition() - centerPos)) > 0;
	bool goodDir = true;

	if (b2Dot(direction, movDir) > 0 && onRightSide)
	{
		goodDir = false;
	}
	else if (b2Dot(direction, movDir) < 0 && !onRightSide)
	{
		goodDir = false;
	}	

	if (furthest > totalMovement/2 && !goodDir)
	{
		body->SetLinearVelocity(-body->GetLinearVelocity() * 0.20f);

		if (onRightSide)
		{
			acceptRightInput = false;
		}
		else
		{
			acceptLeftInput = false;
		}
	}
}
