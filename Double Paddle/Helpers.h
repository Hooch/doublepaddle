#pragma once
#include <SFML/System.hpp>
#include <Box2D/Box2D.h>
#include <math.h>

#define PI 3.14159265358979323846
#define DEG2RAD(deg) (deg * PI / 180.0)
#define RAD2DEG(rad) (rad * 180.0 / PI)

static sf::Vector2f sfVectorFromB2Vector(const b2Vec2& b2)
{
	return{ b2.x, b2.y };
}

static b2Vec2 b2VectorFromSfVector(const sf::Vector2f& sf)
{
	return{ sf.x, sf.y };
}

static void Rotateb2VecBy(b2Vec2& vec, float32 rad)
{
	float newX = std::cosf(rad) * vec.x - std::sinf(rad) * vec.y;
	float newY = std::sinf(rad) * vec.x + std::cosf(rad) * vec.y;

	vec.x = newX;
	vec.y = newY;
}

static sf::Vector2f operator/(const sf::Vector2f& vec, float num)
{
	return{ vec.x / num, vec.y / num };
}