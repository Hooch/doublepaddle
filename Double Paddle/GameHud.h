#pragma once

#include <SFML\Graphics.hpp>
#include <string>
#include <sstream>

class GameHud
{
public:
	GameHud();
	~GameHud();

	void Update(const sf::Time& currentTime, const sf::Time& dTime);
	void Draw(sf::RenderWindow& renderWindow);

	void SetScore(int score);
	void SetBallsUsed(int ballsLost);
	void SetViewport(const sf::FloatRect& rect);

private:
	int score;
	int ballsLost;

	sf::Font scoreFont;
	sf::Text scoreText;
	sf::Text ballsUsedText;
	sf::View hudView;
	sf::RectangleShape background;
};