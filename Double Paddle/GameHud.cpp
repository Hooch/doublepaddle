#include "GameHud.h"

GameHud::GameHud() : score(0), ballsLost(0), hudView({0, 0, 500.0, 1500.0}), background(hudView.getSize())
{
	scoreFont.loadFromFile("Assets\\Fonts\\JLSSpaceGothicR_NC.otf");
	const int FONT_SIZE = 80;
	const int NEXT_LINE = FONT_SIZE + 10;
	int currentLine = 0;

	scoreText.setFont(scoreFont);
	scoreText.setColor(sf::Color::White);
	scoreText.setCharacterSize(FONT_SIZE);
	scoreText.setPosition(20, currentLine);

	ballsUsedText.setFont(scoreFont);
	ballsUsedText.setColor(sf::Color::White);
	ballsUsedText.setPosition(20, currentLine += NEXT_LINE * 2);
	ballsUsedText.setCharacterSize(FONT_SIZE);

	background.setFillColor(sf::Color(120, 120, 120, 255));
	background.setPosition(0, 0);
}

GameHud::~GameHud()
{

}

void GameHud::Update(const sf::Time& currentTime, const sf::Time& dTime)
{
	std::ostringstream oss;
	oss << "Score:\n" << score;
	scoreText.setString(oss.str());

	oss.str(std::string());
	oss << "Balls lost:\n" << ballsLost;
	ballsUsedText.setString(oss.str());
}

void GameHud::Draw(sf::RenderWindow& renderWindow)
{
	auto oldView = renderWindow.getView();
	renderWindow.setView(hudView);

	renderWindow.draw(background);
	renderWindow.draw(scoreText);
	renderWindow.draw(ballsUsedText);

	//Restore
	renderWindow.setView(oldView);
}

void GameHud::SetScore(int score)
{
	this->score = score;
}

void GameHud::SetBallsUsed(int ballsLost)
{
	this->ballsLost = ballsLost;
}

void GameHud::SetViewport(const sf::FloatRect& rect)
{
	hudView.setViewport(rect);
}
