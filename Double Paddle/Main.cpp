#define _CRTDBG_MAP_ALLOC
#include <Windows.h>

#include "Game.h"

void GlobalInitialization()
{
#ifdef _DEBUG
	AllocConsole();
	freopen("CONOUT$", "wb", stdout);
	freopen("CONOUT$", "w", stderr);

#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
}

void GlobalDeintialization()
{
#ifdef _DEBUG
	_CrtDumpMemoryLeaks();
	fclose(stdout);
	fclose(stderr);
	FreeConsole();
#endif
}

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	GlobalInitialization();

	Game game;
	game.Start();

	GlobalDeintialization();
	return EXIT_SUCCESS;
}