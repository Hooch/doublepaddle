#include "BallEntity.h"

BallEntity::BallEntity(float targetSpeed)
: targetSpeed(targetSpeed), size(0.075f), initialMovDir({ 0, 0 })
{	
	shape.setRadius(size / 2);
	shape.setOrigin(size / 2, size / 2);
	shape.setFillColor(sf::Color::White);	
}

EntityType BallEntity::GetEntityType()
{
	return EntityType::ET_BALL;
}

void BallEntity::Update(const sf::Time& currentTime, const sf::Time& dTime)
{
	b2Vec2 movDir = body->GetLinearVelocity();
	if (movDir.x == 0 && movDir.y == 0)
	{
		movDir = initialMovDir;
	}
	movDir.Normalize();
	
	movDir *= targetSpeed;
	body->SetLinearVelocity(movDir);
}

void BallEntity::Draw(sf::RenderWindow& renderWnd)
{
	shape.setPosition(sfVectorFromB2Vector(body->GetPosition()));
	renderWnd.draw(shape);
}

void BallEntity::SetTargetSpeed(float speed)
{
	targetSpeed = speed;
}

float BallEntity::GetTargetSpeed()
{
	return targetSpeed;
}

void BallEntity::SetSize(float size)
{
	b2Vec2 oldDir = body->GetLinearVelocity();
	oldDir.Normalize();
	oldDir *= targetSpeed;
}

void BallEntity::CreateB2BallAndRegister(const b2Vec2& pos, const b2Vec2& dir, b2World& world)
{
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position = pos;
	bodyDef.linearDamping = 0;
	bodyDef.fixedRotation = false;
	bodyDef.bullet = true;

	body = world.CreateBody(&bodyDef);

	b2CircleShape shape;
	shape.m_radius = size / 2;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.restitution = 1;
	fixtureDef.density = BALL_DENSITY;
	fixtureDef.userData = this;

	fixture = body->CreateFixture(&fixtureDef);

	body->SetLinearVelocity(dir * targetSpeed);
	initialMovDir = dir;
}

void BallEntity::PostSolve(PhysicsEntity& other, b2Contact* contact, const b2ContactImpulse* impulse)
{
	if (other.GetEntityType() == ET_PADDLE)
	{
		b2Vec2 movDir = body->GetLinearVelocity();
		if (movDir.x == 0 && movDir.y == 0)
		{
			movDir = initialMovDir;
		}
		movDir.Normalize();

		float randomAngle = -PADDLE_RANDOM_BOUNCE + (float)(rand()) / ((float)(RAND_MAX / (PADDLE_RANDOM_BOUNCE - (-PADDLE_RANDOM_BOUNCE))));
		Rotateb2VecBy(movDir, randomAngle);

		movDir *= targetSpeed;
		body->SetLinearVelocity(movDir);
	}
}
