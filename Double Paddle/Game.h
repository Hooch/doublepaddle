#pragma once
#include <SFML/Graphics.hpp>

#include "GlobalDefines.h"
#include "SceneManager.h"
#include "StartScreenScene.h"

class Game
{
public:
	Game();
	~Game();
	void Start();
	Game(const Game&) = delete;

private:
	void CreateRenderWindow(int w, int h, int posX, int posY);
	void MainLoop();

private:
	sf::RenderWindow* window;
	SceneManager* sceneManager;

	bool isQuiting;
	sf::Time gameTime;
	sf::Clock gameClock;
};