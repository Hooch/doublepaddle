#include "PhysicsEntity.h"

b2Body& PhysicsEntity::GetBody()
{
	return *body;
}

b2Fixture& PhysicsEntity::GetFixture()
{
	return *fixture;
}

sf::Vector2f PhysicsEntity::GetPosition()
{
	return sfVectorFromB2Vector(body->GetPosition());
}

float PhysicsEntity::GetRotationDeg()
{
	return (float)RAD2DEG(body->GetAngle());
}

float PhysicsEntity::GetRotationRad()
{
	return body->GetAngle();
}

PhysicsEntity::~PhysicsEntity()
{
	if (body)
	{
		body->GetWorld()->DestroyBody(body);
	}
}

void PhysicsEntity::EndContact(PhysicsEntity& other, b2Contact* contact)
{
	for (auto callback : endContactCallbacks)
	{
		callback(contact, this, &other);
	}
}

void PhysicsEntity::PostSolve(PhysicsEntity& other, b2Contact* contact, const b2ContactImpulse* impulse)
{
	for (auto callback : postSolveCallbacks)
	{
		callback(contact, impulse, this, &other);
	}
}

void PhysicsEntity::AddEndContactCallback(EndContactCallback callback)
{
	endContactCallbacks.push_back(callback);
	endContactCallbacks.unique();
}

void PhysicsEntity::RemoveEndContactCallback(EndContactCallback callback)
{
	endContactCallbacks.remove(callback);
}

void PhysicsEntity::AddPostSolveCallback(PostSolveCallback callback)
{
	postSolveCallbacks.push_back(callback);
	postSolveCallbacks.unique();
}

void PhysicsEntity::RemovePostSolveCallback(PostSolveCallback callback)
{
	postSolveCallbacks.remove(callback);
}
